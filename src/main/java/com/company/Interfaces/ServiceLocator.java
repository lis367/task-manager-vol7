package com.company.Interfaces;


import com.company.entity.User;

public interface ServiceLocator {


Service getService(String name);
void setUser(User user);
User getUser();


}
