package com.company.service;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Project;
import com.company.Interfaces.Service;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public final class ProjectServiceImpl implements ProjectServiceInterface, Service {

    private ProjectRepoInterface projectRepository;
    private TaskRepoInterface taskRepository;
    private Bootstrap bootstrap;

    public String projectCreate(String name, Date dateStart, Date dateEnd, String userId) {
        String id = UUID.randomUUID().toString();
        Project project = new Project(name, id, userId);
        project.setDateBegin(dateStart);
        project.setDateEnd(dateEnd);
        projectRepository.persist(project);
        return project.getId();
    }

    public ArrayList projectList() {
        return projectRepository.findAll(bootstrap.getUser().getUserId());
    }

    public void projectClear() {
        projectRepository.removeAll();
    }

    public void projectRemove(String id) throws ObjectIsNotFound {
        if(projectRepository.findOne(id)==null){
                throw new ObjectIsNotFound();
        }
        else {
            projectRepository.remove(id);
            ArrayList<String>copyofTask = taskRepository.getTasksFromProject(id);
            for(int i=0;i<copyofTask.size();i++){
                taskRepository.remove(copyofTask.get(i));
            }
        }
    }

    public Project read(String id) {
        return projectRepository.findOne(id);
    }


    public void update(Project project){
        projectRepository.merge(project);
    }

    public ProjectRepoInterface getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepoInterface projectRepository) {
        this.projectRepository = projectRepository;
    }

    public TaskRepoInterface getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepoInterface taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public String getName() {
        return "Project-Service";
    }

}
