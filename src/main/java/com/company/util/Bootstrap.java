package com.company.util;

import com.company.Interfaces.*;
import com.company.commands.AboutCommand;
import com.company.commands.AbstractCommand;
import com.company.commands.HelpCommand;
import com.company.commands.projectCommands.*;
import com.company.commands.taskCommands.*;
import com.company.commands.userCommands.*;
import com.company.entity.*;
import com.company.exception.CommandCorruptException;
import com.company.repository.*;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    private final ServiceCache cache = new ServiceCache();

    private User user;

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    @Override
    public Service getService(String name) {
        Service service = cache.getService(name);
        if(service!=null){
            return service;
        }
        InitialService initialService = new InitialService();
        Service service1 = (Service) initialService.lookup(name);
        cache.addService(service1);
        return service1;
    }

    public void start() throws Exception {
        execute("user-load");
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
            System.out.println();
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            return;}
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            return; }
        if ((user==null)&&(abstractCommand.secureCommand())){
            System.out.println("SECURE COMMAND. YOU NEED TO LOG IN");
        }
        else{
        abstractCommand.execute();}
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws CommandCorruptException {

        serviceAndRepoInit();
        commands.clear();
        // Созданы комманды и загружены в лист Commands через registry
        for (AbstractCommand commands: commandsInititizalization()){
            registry(commands);
        }

    }

    public AbstractCommand[] commandsInititizalization(){
        AbstractCommand[] commandsInit = {
                new HelpCommand(this), new AboutCommand(this), new ProjectClearCommand(this),
                new ProjectCreateCommand(this), new ProjectListCommand(this), new ProjectUpdateCommand(this),
                new ProjectRemoveCommand(this), new ProjectViewCommand(this), new TaskClearCommand(this),
                new TaskCreateCommand(this), new TaskListCommand(this), new TaskRemoveCommand(this),
                new TaskUpdateCommand(this), new TaskViewCommand(this),
                new UserAuthorizationCommand(this), new UserEndSessionCommand(this), new UserRegistrationCommand(this),
                new UserUpdateCommand(this), new UserLoadFromDBCommand(this), new UserUpdatePasswordCommand(this),
                new UserViewCommand(this), new UserDeleteCommand(this),

        };

        return commandsInit;
    }

    // Созданы Проджект,Таск,Юзер сервисы/репозитории
    public void serviceAndRepoInit(){

        final ProjectServiceInterface projectService = (ProjectServiceInterface) getService("Project-Service");
        final TaskServiceInterface taskService = (TaskServiceInterface) getService("Task-service");
        final Map<String, Project> projectRepositoryMap = new HashMap<>();
        final Map<String, Task> taskRepositoryMap = new HashMap<>();
        final TaskRepoInterface taskRepository = new TaskRepositoryImpl(taskRepositoryMap);
        final ProjectRepoInterface projectRepository = new ProjectRepositoryImpl(projectRepositoryMap,taskRepository);

        projectService.setProjectRepository(projectRepository);
        projectService.setTaskRepository(taskRepository);
        projectService.setBootstrap(this);
        taskService.setTaskRepository(taskRepository);
        taskService.setProjectService(projectService);
        taskService.setBootstrap(this);

        final Map<String,User> userRepositoryMap = new HashMap<>();
        final UserRepoInterface userRepository = new UserRepositoryImpl(userRepositoryMap);
        final UserServiceInterface userServiceImpl = (UserServiceInterface) getService("User-service");
        userServiceImpl.setUserRepository(userRepository);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
