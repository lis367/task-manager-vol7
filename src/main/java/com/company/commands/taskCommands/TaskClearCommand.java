package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;


public final class TaskClearCommand extends AbstractCommand {

    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        taskServiceImpl.taskClear();
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskServiceInterface taskServiceImpl) {
        this.taskServiceImpl = taskServiceImpl;
    }

    public TaskClearCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

}
