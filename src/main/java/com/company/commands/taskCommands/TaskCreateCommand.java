package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TaskServiceImpl;
import com.company.util.Bootstrap;

import java.lang.invoke.SerializedLambda;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public final class TaskCreateCommand extends AbstractCommand {

    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine().trim();
        if(line.isEmpty()){
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String projectID = sc.nextLine().trim();
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        String dateStart = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        String dateEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try{
            System.out.println("TASK ID IS");
            System.out.println(taskServiceImpl.taskCreate(line , projectID, dateFormatter.parse(dateStart) , dateFormatter.parse(dateEnd), serviceLocator.getUser().getUserId()) );
        }
        catch (ParseException e){
            System.out.println("Wrong format dd.mm.yyyy");
        }
        catch (ObjectIsNotFound e){
            System.out.println("Такого проекта не сущесвует");
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskServiceInterface taskServiceImpl) {
        this.taskServiceImpl = taskServiceImpl;
    }

    public TaskCreateCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
