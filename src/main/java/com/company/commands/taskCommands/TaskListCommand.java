package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.Task;


import java.util.ArrayList;

public final class TaskListCommand extends AbstractCommand {

    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        System.out.println("[TASK LIST]");
        ArrayList<Task> tasksPool = taskServiceImpl.taskList();
        for(int i=0;i<tasksPool.size();i++){
            System.out.println("Task Name: "+tasksPool.get(i).getName() + " Task ID: " + tasksPool.get(i).getId() + " Project :"+tasksPool.get(i).getProjectID());
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskServiceInterface taskServiceImpl) {
        this.taskServiceImpl = taskServiceImpl;
    }

    public TaskListCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

}
