package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.Task;


import java.text.SimpleDateFormat;
import java.util.Scanner;

public final class TaskUpdateCommand extends AbstractCommand {
    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if (taskServiceImpl.read(line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                Task taskPool = taskServiceImpl.read(line);
                System.out.println("ENTER NEW NAME");
                line = sc.nextLine();
                taskPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
                String dateStart = sc.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE TASK");
                String dateEnd = sc.nextLine().trim();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                taskPool.setDateBegin(dateFormatter.parse(dateStart));
                taskPool.setDateEnd(dateFormatter.parse(dateEnd));
                taskServiceImpl.update(taskPool);
                System.out.println("SUCCESS");
            } else {
                System.out.println("no rights to update tasks of other users");
            }
        }
        catch (NullPointerException e){
            System.out.println("Task is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskUpdateCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
