package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;


import java.util.Scanner;

public final class TaskRemoveCommand extends AbstractCommand {

    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if(taskServiceImpl.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
            taskServiceImpl.taskRemove(line);
            System.out.println("TASK REMOVED");}
            else {
                System.out.println("no rights to delete task of other users");
            }
        }
        catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
        }


    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskServiceInterface taskServiceImpl) {
        this.taskServiceImpl = taskServiceImpl;
    }

    public TaskRemoveCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }


}
