package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;


public final class UserEndSessionCommand extends AbstractCommand {
    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;


    @Override
    public String command() {
        return "user-end";
    }

    @Override
    public String description() {
        return "End of User session";
    }

    @Override
    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("LOG OUT SUCCESSFUL");
        serviceLocator.setUser(null);
    }
    public boolean secureCommand() {
        return true;
    }

    public UserEndSessionCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

    public void setUserService(UserServiceInterface userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }
}
