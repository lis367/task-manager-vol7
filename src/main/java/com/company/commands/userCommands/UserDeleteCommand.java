package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.service.UserServiceImpl;


import java.util.Scanner;

public final class UserDeleteCommand extends AbstractCommand {

    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;


    @Override
    public String command() {
        return "user-delete";
    }

    @Override
    public String description() {
        return "Delete user from repository";
    }

    @Override
    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("ENTER LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userServiceImpl.generateMD5(sc.nextLine());;
        User user = userServiceImpl.read(login,password);
        if(user.getUserId().equals(serviceLocator.getUser().getUserId())){
            userServiceImpl.remove(user);
            System.out.println("Profile deleted successfully");
        }
        else{
            System.out.println("no rights to delete profile of other users");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserServiceInterface getUserService() {
        return userServiceImpl;
    }

    public void setUserService(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    public ServiceLocator getBootstrap() {
        return serviceLocator;
    }

    public void setBootstrap(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

    public UserDeleteCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
