package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;


import java.util.Scanner;

public final class UserUpdateCommand extends AbstractCommand {


    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;


    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update User";
    }

    @Override
    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("ENTER NEW LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER NEW PASSWORD");
        String password = userServiceImpl.generateMD5(sc.nextLine());
        User user = serviceLocator.getUser();
        user.setName(login);
        user.setPassword(password);
        serviceLocator.setUser(user);
        userServiceImpl.update(user);

    }

    public boolean secureCommand() {
        return true;
    }

    public UserUpdateCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

    public void setUserService(UserServiceInterface userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

}
