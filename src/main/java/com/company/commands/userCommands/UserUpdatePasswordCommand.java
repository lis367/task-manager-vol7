package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;


import java.util.Scanner;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;


    @Override
    public String command() {
        return "user-newPassword";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("ENTER NEW PASSWORD");
        Scanner sc = new Scanner(System.in);
        String password = userServiceImpl.generateMD5(sc.nextLine());
        User user = serviceLocator.getUser();
        user.setPassword(password);
        serviceLocator.setUser(user);
        userServiceImpl.update(serviceLocator.getUser());
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserUpdatePasswordCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }

    public void setUserService(UserServiceInterface userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    public void setServiceLocator(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
