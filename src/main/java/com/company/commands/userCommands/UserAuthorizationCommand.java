package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;


import java.util.Scanner;

public final class UserAuthorizationCommand extends AbstractCommand  {

    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;


    public String command() {
        return "user-authorization";
    }


    public String description() {
        return "Authorization";
    }


    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("ENTER LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userServiceImpl.generateMD5(sc.nextLine());
        if((userServiceImpl.read(login,password))==null){
            System.out.println("USER NOT FOUND");
            System.out.println("Registration:");
            userServiceImpl.registration();
        }
        else {
            System.out.println("Authorization success");
            serviceLocator.setUser(userServiceImpl.read(login,password));
        }

    }
    public boolean secureCommand() {
        return false;
    }


    public UserAuthorizationCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }


    public void setUserService(UserServiceInterface userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    public void setBootstrap(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
