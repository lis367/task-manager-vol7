package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.entity.UserRoleType;


import java.util.Scanner;
import java.util.UUID;

public final class UserRegistrationCommand extends AbstractCommand {

    private UserServiceInterface userServiceImpl;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "user-registration";
    }

    @Override
    public String description() {
        return "Registration";
    }

    @Override
    public void execute() throws Exception {
        userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        System.out.println("ENTER LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userServiceImpl.generateMD5(sc.nextLine());
        User user = new User(login,password);
        String id =  UUID.randomUUID().toString();
        user.setUserId(id);
        user.setUserRoleType(UserRoleType.USER);
        userServiceImpl.save(user);
        System.out.println("User id = "+ id + " with login "+login +" password "+password);
    }

    public boolean secureCommand() {
        return false;
    }


    public UserRegistrationCommand(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    public void setUserService(UserServiceInterface userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

}
