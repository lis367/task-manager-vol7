package com.company.commands;


public abstract class AbstractCommand {
    public abstract String command();
    public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean secureCommand();
}

