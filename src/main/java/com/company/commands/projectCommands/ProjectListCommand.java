package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;

import java.util.ArrayList;

public final class ProjectListCommand extends AbstractCommand {

    private ProjectServiceInterface projectService;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() {
        projectService = (ProjectServiceInterface) serviceLocator.getService("Project-service");
        System.out.println("[PROJECT LIST]");
        ArrayList <Project> projectPool = projectService.projectList();
        for(int i=0; i<projectPool.size();i++){
            System.out.println("Project Name: "+projectPool.get(i).getName() + " Project ID: " + projectPool.get(i).getId());
         }

    }
    public boolean secureCommand() {
        return true;
    }

    public void setProjectService(ProjectServiceInterface projectService) {
        this.projectService = projectService;
    }

    public ProjectListCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
}
