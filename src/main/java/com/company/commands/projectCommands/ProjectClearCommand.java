package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;

public final class ProjectClearCommand extends AbstractCommand {

    private ProjectServiceInterface projectService;
    private TaskServiceInterface taskServiceImpl;
    private ServiceLocator serviceLocator;



    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-service");
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        projectService.projectClear();
        taskServiceImpl.taskClear();
        System.out.println("ALL PROJECT&TASK REMOVED");}
        else {
            System.out.println("ADMIN ROLETYPE ARE REQUIRED");
        }
    }

    public void setProjectService(ProjectServiceInterface projectService) {
        this.projectService = projectService;
    }

    public void setTaskService(TaskServiceInterface taskServiceImpl) {
        this.taskServiceImpl = taskServiceImpl;
    }


    public ProjectClearCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;
    }
    public boolean secureCommand() {
        return true;
    }




}
