package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;


import java.util.Scanner;

public final class ProjectRemoveCommand extends AbstractCommand {

    private ProjectServiceInterface projectService;
    private ServiceLocator serviceLocator;

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if(projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
            projectService.projectRemove(line);
                System.out.println("PROJECT&TASK REMOVED");

            }
            else{
                System.out.println("no rights to delete project of other users");
            }
        } catch (ObjectIsNotFound objectIsNotFound) {
            System.out.println("PROJECT IS NOT FOUND");
        }


    }
    public boolean secureCommand() {
        return true;
    }

    public void setProjectService(ProjectServiceInterface projectService) {
        this.projectService = projectService;
    }

    public ProjectRemoveCommand(ServiceLocator bootstrap) {
        this.serviceLocator = bootstrap;

    }

}
